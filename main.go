package main

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

var urls = [4]string{"https://u0e8utqkk2.execute-api.eu-west-2.amazonaws.com/dev/transaction-monitor/health",
	               	 "https://u0e8utqkk2.execute-api.eu-west-2.amazonaws.com/dev/microservice-controller/health",
					 "https://u0e8utqkk2.execute-api.eu-west-2.amazonaws.com/dev/email-service/health",
					 "https://u0e8utqkk2.execute-api.eu-west-2.amazonaws.com/dev/payment-gateway/health"}


// flow
//
// read yaml with urls (later) and cron interval
// cron job call endpoints (using channels go)
// collect http status codes (try HEAD instead GET)
// try parse response messages (xml, json)
// persist as a timeseries (HeapMap) all responses
// query persistence via HTTP healthcheck endpoint


type Probe struct {
	name string
	checkedAt int64
	statusCode int
	body  string
}

func MakeHttpRequest(u string, ch chan <- Probe) {

	parsedUrl, _ := url.Parse(u)
	start := time.Now()
	resp, _ := http.Get(u)
	body, _ := ioutil.ReadAll(resp.Body)

	ch <- Probe{
		name:       parsedUrl.Path,
		checkedAt:  start.Unix(),
		statusCode: resp.StatusCode,
		body:       string(body),
	}
}


func main() {

		start := time.Now()
		ch := make(chan Probe)

		fmt.Printf("%.2fs elapsed\n", time.Since(start).Seconds())

		c := cron.New(cron.WithSeconds())

		_, err := c.AddFunc("@every 5s", func() {

			for _, u := range urls {
				go MakeHttpRequest(u, ch)
			}

			for range urls {
				fmt.Println(<-ch)
			}
		})

		if err != nil {
			fmt.Println(err)
		}

	for {

		c.Start()
	}
}
